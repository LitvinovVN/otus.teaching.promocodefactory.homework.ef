﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public interface IDbInitializer
    {
        /// <summary>
        /// Инициализация базы данных
        /// </summary>
        public void InitializeDb();
    }
}
