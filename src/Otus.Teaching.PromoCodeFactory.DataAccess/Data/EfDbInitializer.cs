﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class EfDbInitializer : IDbInitializer
    {
        private readonly DbContextSqlite _dbContext;

        public EfDbInitializer(DbContextSqlite dbContext)
        {
            _dbContext = dbContext;
        }
        public void InitializeDb()
        {
            _dbContext.Database.EnsureDeleted();
            //_dbContext.Database.EnsureCreated();
            _dbContext.Database.Migrate();
                        
            //_dbContext.AddRange(FakeDataFactory.Roles);
            //_dbContext.SaveChanges();

            _dbContext.AddRange(FakeDataFactory.Employees);
            _dbContext.SaveChanges();

            _dbContext.AddRange(FakeDataFactory.Preferences);
            _dbContext.SaveChanges();

            _dbContext.AddRange(FakeDataFactory.Customers);
            _dbContext.SaveChanges();            
        }
    }
}
