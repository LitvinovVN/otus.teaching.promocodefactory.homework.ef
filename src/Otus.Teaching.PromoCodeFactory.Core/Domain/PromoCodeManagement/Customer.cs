﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        :BaseEntity
    {
        [MaxLength(20, ErrorMessage = "Длина строки ограничена 20 символами!")]
        public string FirstName { get; set; }

        [MaxLength(20, ErrorMessage = "Длина строки ограничена 20 символами!")]
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }

        //TODO: Списки Preferences и Promocodes 
        public ICollection<CustomerPreference> Preferences { get; set; }

        public ICollection<PromoCode> PromoCodes { get; set; }
    }
}